<?php

@extends('index')
@section('title', 'projects')
@section('content')
    <div class="container mt-5">
        <h2>Projects</h2>
        <table class="table mt-3">
            <thead>
            <tr>
                <th scope="col">Department</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Start date - End date</th>
                <th scope="col">add task</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($projects as $project)
                <tr>
                    <td>{{ $project->department->department['name'] }}</td>

                    <td>{{ $projet->title }}</td>
                    <td>{{ $projet->description }}</td>
                    <td>{{ $projet->startdate }} - {{ $projet->enddate }}</td>
                    <td>
                    <a href="/tasks/{{ $project->id }}/create" class="btn btn-sm btn-success">Add task to this project</a> |

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(!empty(Session::get('success')))
            <div class="alert alert-success"> {{ Session::get('success') }}</div>
        @endif
        @if(!empty(Session::get('error')))
            <div class="alert alert-danger"> {{ Session::get('error') }}</div>
        @endif
    </div>
@endsection
