<!-- resources/views/dashboard/reservationCreate.blade.php -->
@extends('index')
@section('title', 'Create task')
@section('content')
    <div class="container my-5">
        <div class="card">
            <div class="card-header">
                <h2>{{ $projectInfo->title }} - <small class="text-muted">{{ $projectInfo->startdate }}</small></h2>
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text">Create your task</p>
                <form action="{{ route('tasks.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="room">Select project department</label>
                                <select class="form-control" name="room_id">
                                    @foreach ($projectInfo->names as $option)
                                        <option value="{{$option->id}}">{{ $option->name }} - ${{ $option->location }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="num_of _persons">Number of people</label>
                                <input class="form-control" name="num_of_persons" placeholder="1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="startdate">Start Date</label>
                                <input type="date" class="form-control" name="startdate" placeholder="02/20/2020">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="enddate">End</label>
                                <input type="date" class="form-control" name="enddate" placeholder="12/20/2020">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary">Create </button>
                </form>
            </div>
        </div>
    </div>
@endsection
