<?php
@extends('index')
@section('title', 'categories')
@section('content')
    <div class="container mt-5">
        <h2>Task categories</h2>
        <table class="table mt-3">
            <thead>
            <tr>
                <th scope="col">Category</th>
              <th scope="col">Department</th>
                <th scope="col">Department ID</th>
                <th scope="col">add/update</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->department->department['name'] }}</td>

                    <td>{{ $category->department['ID'] }}</td>

                    <td><a href="/tasks/{{ $category->id }}/edit" class="btn btn-sm btn-success">Edit</a></td>
                    <td><a href="/tasks/{{ $category->id }}/create" class="btn btn-sm btn-success">Create</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(!empty(Session::get('success')))
            <div class="alert alert-success"> {{ Session::get('success') }}</div>
        @endif
        @if(!empty(Session::get('error')))
            <div class="alert alert-danger"> {{ Session::get('error') }}</div>
        @endif
    </div>
@endsection
=======

