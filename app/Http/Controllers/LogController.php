<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Task;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    /**
     * Display a listing of the tasks.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $user_id = Auth::user()->id;
        /* $tasks = Task::with('project', 'project.department')
             ->where('user_id', $user_id)
             ->orderBy('created_at', 'asc')
             ->get();
        */
        $query = "SELECT tasks.* , p.title AS projectName FROM projects p JOIN tasks ON projects.id = tasks.project_id";
        $results = \DB::select($query);

        return view('task.tasks')->with('tasks', $results);
    }

    /**
     * Show the form for creating a new log.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($project_id)
    {
        // $query = "SELECT tasks.* , p.title AS projectName FROM projects p JOIN tasks ON projects.id = tasks.project_id";
        // $results = \DB::select($query);

        $projectInfo = Project::with('tasks')->get()->find($project_id);
        return view('task.taskSelectForm', compact('projectInfo'));
    }

    /**
     * Store a newly created log in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = Auth::user()->id;
        $request->request->add(['user_id' => $user_id]);

        Log::create($request->all());

        return redirect('task/logs')->with('success', 'Log submitted!');
    }

    /**
     * Display the specified log.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(Log $log)
    {

        $user_id = Auth::user()->id;
        $log = Log::with('room', 'room.hotel')
            ->get()
            ->find($log->id);
        //user_id are not the same type at this design
        //  if ($log->user_id === $user_id) {
        if ($log->user_id == $user_id) {
            $project_id = $log->task->project_id;
            $projectInfo = Project::with('tasks')->get()->find($project_id);

            return view('task.taskSingle', compact('log', 'projectInfo'));
        }
        else
            return redirect('task/logs')->with('error', 'You are not authorized to see that.');
    }

    /**
     * Show the form for editing the specified log.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Log $log)
    {


        $user_id = Auth::user()->id;
        $log = Log::with('log', 'log.task')
            ->get()
            ->find($log->id);
        //user_id are not the same type at this design
        //  if ($log->user_id === $user_id) {
        if ($log->user_id == $user_id) {
            $project_id = $log->task->Project_id;
            $projectInfo = Project::with('tasks')->get()->find($project_id);

            return view('task.logEdit', compact('log', 'projectInfo'));
        } else
            return redirect('task/logs')->with('error', 'You are not authorized.');
    }

    /**
     * Update the specified log in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Log $log)
    {

        $user_id = Auth::user()->id;
        if ($log->user_id != $user_id){
            return redirect('task/logs')->with('error', 'You are not authorized to update this log');
        }else {
            //  $user_id = \Auth::user()->getUserInfo()['sub'];
            $log->user_id = $user_id;
            $log->num_of_persons = $request->num_of_persons;
            $log->start_date = $request->start_date;
            $log->end_date = $request->end_date;
            $log->title = $request->title;
            $log->description = $request->description;
            $log->save();

            return redirect('task/logs')->with('success', 'Successfully updated your log!!');
        }
    }

    /**
     * Remove the specified log from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $log)
    {

        $user_id = Auth::user()->id;
        $log = Reservation::find($log->id);
        //user_id are not the same type at this design
        //  if ($log->user_id === $user_id) {
        if ($log->user_id == $user_id) {
            $log->delete();

            return redirect('task/logs')->with('success', 'Successfully deleted your log!');
        } else
            return redirect('task/logs')->with('error', 'You are not authorized to delete this log');
    }
}
