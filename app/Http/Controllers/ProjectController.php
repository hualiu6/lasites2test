<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

class ProjectController extends Controller
{
    //
    public function index() {

      //  $projects = Project::all();

       // $projects = DB::table('projects')
        //    ->join('departments', 'projects.department_id', '=', 'departments.id')
         //   ->join('orders', 'users.id', '=', 'orders.user_id')
          //  ->select('users.*', 'contacts.phone', 'orders.price')
        //    ->get();

        $query = "SELECT projects.* , d.name AS departmentName FROM departments d JOIN projects ON projects.department_id = d.id";
        $results = \DB::select($query);
        // dump($result);
        return view('projects')->with('projects', $results);
      //  return view('projects')->with('projects', $projects);
    }
}
