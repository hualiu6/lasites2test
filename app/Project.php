<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
   // public $timestamps = false;

    protected $fillable = [
        'user_id',
        'department_id',
        'title',
        'description',
        'num_of_persons',
        'startdate',
        'enddate'
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function Department() {
         return $this->belongsTo('App\Department');
     }

}
