<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'division_id',
        'name',
        'description',
        'location'

    ];

    public function Division() {
       return $this->belongsTo('App\Division');
    }

    public function projects() {
        return $this->hasMany('App\Project');
    }

}
