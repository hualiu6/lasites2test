<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
  //  public $timestamps = false;
    protected $fillable = [
        'task_id',
        'user_id',
        'title',
        'description',
        'start_date',
        'end_date'
    ];

    public function task() {
        return $this->belongsTo('App\Task');
    }
}
