<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'location',
        'description'
    ];

    public function departments() {
       // return $this->hasMany('App\Department');
        return $this->hasMany(Department::class);
    }

}
