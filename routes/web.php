<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('login', 'AuthController@index');
Route::get('/login', 'AuthController@index')->name('login');

Route::post('/post-login', 'AuthController@postLogin');
Route::get('/registration', 'AuthController@registration');
Route::post('/post-registration', 'AuthController@postRegistration');
//Route::get('/dashboard', 'AuthController@dashboard');
//Route::get('logout', 'AuthController@logout');
Route::get('/logout', 'AuthController@logout')->name('logout')->middleware('auth');

/* hotel auth0 example

Route::view('/', 'home');
Route::get('/hotels', 'HotelController@index');
Route::get('/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name('auth0-callback');
Route::get('/login', 'Auth\Auth0IndexController@login')->name('login');
Route::get('/logout', 'Auth\Auth0IndexController@logout')->name('logout')->middleware('auth');

Route::group(['prefix' => 'dashboard'], function() {
    Route::view('/', 'dashboard/dashboard');
    Route::get('reservations/create/{id}', 'ReservationController@create');
    Route::resource('reservations', 'ReservationController')->except('create');
});
*///

//Hotel Routes
Route::middleware('auth')->group(function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::view('/', 'dashboard/dashboard');
        Route::get('reservations/create/{id}', 'ReservationController@create');
        Route::resource('reservations', 'ReservationController')->except('create');
    });
});

Route::post('jwtlogin', 'UserController@authenticate');

Route::view('/', 'home');
Route::get('/hotels', 'HotelController@index');
Route::get('/projects', 'ProjectController@index');

//add route to add tasks
//Route::group(['middleware' => 'auth.jwt'], function () {

  //  Route::post('task/{id}', 'TaskController@create');
   // Route::put('task/{id}', 'TaskController@update');

    Route::group(['prefix' => 'task'], function () {
       // Route::view('/', 'task');
        Route::get('logs/create/{id}', 'LogController@create');
        Route::resource('logs', 'LogController')->except('create');
    });
//});
