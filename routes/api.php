<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('jwtregister', 'AuthController@register');
//use form registration
/*
Route::post('jwtregister', 'AuthController@register');
Route::post('jwtlogin', 'AuthController@login');
Route::get('jwtlogout', 'AuthController@logout');
Route::get('user', 'AuthController@getAuthUser');
*/

//JWT authentication
//all the routes we need to test out JWT. Every route we do not wish to secure is kept outside the JWT middleware.
//example: https://blog.pusher.com/laravel-jwt/

Route::post('jwtregister', 'UserController@register');
Route::post('jwtlogin', 'UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('jwtuser', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

//JWT another example auth.jwt https://www.larashout.com/laravel-6-jwt-authentication
Route::post('jwt2login', 'ApiController@login');
Route::post('jwt2register', 'ApiController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('jwt2logout', 'ApiController@logout');

    Route::get('tasks', 'TaskController@index');
    Route::get('tasks/{id}', 'TaskController@show');
    Route::post('tasks', 'TaskController@store');
    Route::put('tasks/{id}', 'TaskController@update');
    Route::delete('tasks/{id}', 'TaskController@destroy');
});
