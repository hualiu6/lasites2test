<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add testing data
        $departments = [
            [
                'division_id' => '1',
                'name' => 'Art',
                'location' => 'Building A',
                'description' => 'The department includes Art, Literature, Music, History, Theater and Dance '

            ],


        ];

        foreach ($departments as $department) {
            Department::create(array(
                'division_id' => $department['division_id'],
                'name' => $department['name'],
                'location' => $department['location'],
                'description' => $department['description']

            ));
        }
    }

}
