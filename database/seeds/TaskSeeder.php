<?php

use Illuminate\Database\Seeder;
use App\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //testing Division data
        $tasks = [
            [
                'project_id' => '1',
                'title' => 'Banner training',
                'description' => 'This is for all employees'
            ],

            [
                'project_id' => '2',
                'title' => 'Banner training 2',
                'description' => 'This is for all employees'
            ],

            [
                'project_id' => '3',
                'title' => 'Banner training 3',
                'description' => 'This is for all employees'
            ],

        ];

        foreach ($tasks as $task) {
            Task::create(array(
                'project_id' => $task['project_id'],
                'title' => $task['title'],
                'description' => $task['description']

            ));
        }
    }
}
