<?php

use Illuminate\Database\Seeder;
//use phpDocumentor\Reflection\Project;
use App\Project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { //add testing data
        $projects = [
            [
                'user_id' => 9996,
                'department_id' => 1,
                'title' => 'Create registration system',
                'num_of_persons' => '36',
                'description' => 'This project is going to update the old version system ',
                'startdate' => '2020-06-29',
                'enddate' => '2029-09-30'


            ],

            [
            'user_id' => '9997',
            'department_id' => '2',
            'title' => 'Create registration system 2',
            'num_of_persons' => '39',
            'description' => 'This project is going to update the old version system 2',
            'startdate' => '2020-07-29',
            'enddate' => '2029-09-30',
            'created_at'=>getdate()

        ],


        ];

        foreach ($projects as $project) {
            Project::create(array(
                'user_id' => $project['user_id'],
                'department_id' => $project['department_id'],
                'title' => $project['title'],
                'num_of_persons' => $project['num_of_persons'],
                'description' => $project['description'],
                'startdate' => $project['startdate'],
                'enddate' => $project['enddate']
            ));
        }
    }
}
