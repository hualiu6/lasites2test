<?php

use App\Division;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //testing Division data
        $divisions = [
            [
                'name' => 'Humanities',
                'location' => 'Building B',
                'description' => 'The division includes Art, Economy, Continue Edu, Literature, Music, History, Theater and Dance'
            ],


        ];

        foreach ($divisions as $division) {
            Division::create(array(
                'name' => $division['name'],
                'location' => $division['location'],
                'description' => $division['description']

            ));
        }
    }
}
