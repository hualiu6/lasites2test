<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaskProjectIdForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //  Schema::enableForeignKeyConstraints();
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->dropColumn('project_id');
          //  $table->foreign('project_id')->references('id')->on('projects');
            //->onDelete('cascade')->onUpdate('cascade');
         //   $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
        });
    }
}
